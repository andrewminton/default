#!/bin/bash
filename="$1"

while IFS="," read one two
do
        if [ "${one:0:1}" != "#" ]; then
        git submodule add -f "$one" public/"$two" --recursive
                echo "Adding $one in public/$two"
    fi
done < "$filename"