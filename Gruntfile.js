module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    shell: {                                // Task
          //command:bash submodules.sh submodules.csv
          multiple:{
              //command: ['git init','bash submodules.sh submodules.csv','bash repoaction.sh' ].join('&&'),
              //Make sure to add username:password and repo username to the below script to ensure to pushes correctly.
              command: ['bash repoaction.sh'].join('&&'),
              options: {
                  stdout: true
              }
          },
          getCWD:{
            command: 'ls',
            options:{
              stdout:true
            }
          },
          gitpush:{
            command: ['git add -A','git commit -m "Package updates"','git push origin master'].join('&&'),
            options: {
              stdout:true
            }

          }

    },
    deployments: {
      options: {
        // any should be defined options here
      },
      // "Local" target
      local: {
        title: 'Local', 
        database: 'local_db_name',        
        user: 'local_db_username',
        pass: 'local_db_password',
        host: 'local_db_host',  
        url: 'local_db_url'
        // note that the `local` target does not have an "ssh_host"
      },
      // "Remote" targets
      develop: {
        title: 'Development', 
        database: 'development_db_name',        
        user: 'development_db_username',
        pass: 'development_db_password',
        host: 'development_db_host',  
        url: 'development_db_url',
        ssh_host: 'ssh_user@ssh_host'
      },
      stage: {
        title: 'Stage', 
        database: 'stage_db_name',        
        user: 'stage_db_username',
        pass: 'stage_db_password',
        host: 'stage_db_host',  
        url: 'stage_db_url', 
        ssh_host: 'ssh_user@ssh_host'  
      },
      production: {
        title: "Production", 
        database: "production_db_name",        
        user: "production_db_username",
        pass: "production_db_password",
        host: "production_db_host",  
        url: "production_db_url",
        ssh_host: "ssh_user@ssh_host"  
      }
    },
    concat: {
      options: {
        separator: ';'
      },
      dist: {
        src: ['src/js/*.js'],
        dest: 'public/workspace/assets/js/<%= pkg.name %>.js'
      }
    },
        less: {
                 options: {     
                        concat: true,
                        relativeUrls : true,
                        paths: "src/css/" // mediaqueries being brought in here << this is needed
                        },
                 components: {
                                files:                          
                                {         'public/workspace/assets/css/<%= pkg.name %>.css': ['src/css/site.less', 'src/css/externals/*.css'],  } // concatenating all css files with the .less file into 1 css
                 }
        },
        recess: {
                        dist: {
                        options: {
                                compile: true
                        },
                        files: {
                                'public/workspace/assets/css/<%= pkg.name %>.css': [
                                        'src/css/styles.less'
                                ]
                        }
                }
        },
        uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n',
                mangle: {
                        except: ['jQuery', 'Backbone']
                }
      },
      dist: {
        files: {
          'public/workspace/assets/js/<%= pkg.name %>.min.js': ['<%= concat.dist.dest %>']
        }
      }
    },
        cssmin: {
                compress: {
                        files: {
                                "public/workspace/assets/css/app.min.css": ["public/workspace/assets/css/app.css"]
                        }
                }
        },
        img: {
        task1: {
            src: 'src/img',
            dest: 'public/workspace/assets/img'
                }
        },
        grunticon: {
      src: "src/css/icons",
      dest: "public/workspace/assets/css/icons"
    },
        env : {
                options : {
                //Shared Options Hash
                },
                dev : {
                  NODE_ENV : 'development',
                  DEST     : 'temp'
                },
                prod : {
                  NODE_ENV : 'production',
                  DEST     : 'dist' 
                }
        },
        preprocess : {

                dev : {

                        src : 'src/utilities/master.xsl',
                        dest : 'public/workspace/utilities/master.xsl'

                },

                prod : {
                        src : 'src/utilities/master.xsl',
                        dest : 'public/workspace/utilities/master.xsl',
                        //src : './src/tmpl/index.html',
                        //dest : '../<%= pkg.version %>/<%= now %>/<%= ver %>/index.html',
                        options : {
                                context : {
                                        name : '<%= pkg.name %>',
                                        version : '<%= pkg.version %>',
                                        now : '<%= now %>',
                                        ver : '<%= ver %>'
                                }

                        }
                }
        },
        htmlmin: {                                    
                prod: {                                      
                  options: {                                 
                        removeComments: true,
                        collapseWhitespace: true
                  },
                  files: {                                   
                        '<%= preprocess.prod.dest %>': '<%= preprocess.prod.dest %>'
                  }
                },
                dev: {                                       
                  files: {
                        '<%= preprocess.prod.dest %>': '<%= preprocess.dev.dest %>'
                  }
                }
        },
        fixturesPath: "fixtures",
    htmlbuild: {
        dist: {
            src: 'index.html',
            dest: 'samples/',
            options: {
                beautify: true,
                scripts: {
                    bundle: [
                        '<%= fixturesPath %>/scripts/*.js',
                        '!**/main.js',
                    ],
                    main: '<%= fixturesPath %>/scripts/main.js'
                },
                styles: {
                    bundle: [ 
                        '<%= fixturesPath %>/css/libs.css',
                        '<%= fixturesPath %>/css/dev.css'
                    ],
                    test: '<%= fixturesPath %>/css/inline.css'
                },
                sections: {
                    views: '<%= fixturesPath %>/views/**/*.html',
                    templates: '<%= fixturesPath %>/templates/**/*.html',
                },
                data: {
                    // Data to pass to templates
                    version: "0.1.0",
                    title: "test",
                },
            }
        }
    },
        /*copy: {
                main: {
                files: [
                {src: ['utilities/**'], dest: 'dest/'}
                ] // includes files in path and its subdirs}
                }
        },*/
        /*template: {
                dev: {
                  src: 'src/template/base.blade',
                  dest: 'public/workspace/utilities/base.xsl',
                  variables: {
                        css_url: 'app.css',
                        title: 'Hello World',
                        pretty: true
                  }
                },
                prod: {
                  src: 'src/template/base-prod.blade',
                  dest: 'public/workspace/utilities/base.xsl',
                  variables: {
                        css_url: 'app.min.css',
                        title: 'Hello Production'
                  }
                },
                dynamicVariables: {
                  src: 'src/template/base.blade',
                  dest: 'public/workspace/utilities/base.xsl',
                  variables: function () {
                        return {
                          css: grunt.file.read('app.min.css'),
                          now: new Date()
                        }
                  }
                }
        },*/
    qunit: {
      files: ['test/**/*.html']
    },
        watch: {
      files: ['gruntfile.js', 'src/**/*.js', 'src/**/*.css', 'src/**/*.less'],
      tasks: ['less', 'concat','cssmin','uglify']
    }
  });
  grunt.loadNpmTasks('grunt-install-dependencies');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-qunit');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('assemble-less');
  grunt.loadNpmTasks('grunt-recess');
  grunt.loadNpmTasks('grunt-img');
  grunt.loadNpmTasks('grunt-env');
  grunt.loadNpmTasks('grunt-preprocess');
  grunt.loadNpmTasks('grunt-templater');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-grunticon');
  grunt.loadNpmTasks('grunt-ftp-deploy');
  grunt.loadNpmTasks('grunt-html-build');
  grunt.loadNpmTasks('grunt-shell');
  grunt.loadNpmTasks('grunt-deployments');


  grunt.registerTask('build',['shell:multiple']);
  grunt.registerTask('gitpush',['shell:gitpush']);
  grunt.registerTask('default',['less','concat','cssmin','uglify','img']);
  grunt.registerTask('dev', ['env:dev','less','concat','preprocess:dev']);
  grunt.registerTask('prod', ['env:prod','cssmin','uglify','img','preprocess:prod']);
  
};